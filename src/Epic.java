import java.util.ArrayList;

/**Epic: holds the epic data type. Contains array of linked stories
 *
 */

public class Epic {
    protected String name;
    protected String CMPS;
    public ArrayList<Story> storyList = new ArrayList<Story>();

    public Epic(String CMPS, String name) {
        this.name = name;
        this.CMPS = CMPS;
    }

    public String getCMPS() {
        return CMPS;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Story> getStoryList() {
        return storyList;
    }

    public void addStory(Story story) {
            storyList.add(story);
    }
}
