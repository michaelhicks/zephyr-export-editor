

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import java.io.*;
import java.util.ArrayList;

public class FileRead {
    public static ArrayList<String> sprintPath = new ArrayList<String>(); //ArrayList holding all sprint paths
    public static String epicPath = "";
    public static String storyPath = "";
    public static String issuePath = "";

    public static ArrayList<Epic> epicList = new ArrayList<Epic>();
    public static ArrayList<Story> storyList = new ArrayList<Story>();

    static FileInputStream file; //This is the Sprint File
    static FileInputStream epicFile;
    static FileInputStream storyFile;
    static FileInputStream issueFile;


    //file_init: loops through the sprint path array list and executes the excel parsing for each one.
    //Breaks if any exception is thrown
    public static int file_init(ArrayList<String> sprintPath, String epicPath, String storyPath, String issuePath) {
        FileRead.epicPath = epicPath;
        FileRead.issuePath = issuePath;
        FileRead.sprintPath = sprintPath;
        FileRead.storyPath = storyPath;
        int numFiles = sprintPath.size();
        for (int i = 0; i < numFiles; i++) {
            try {
                executeExcelParsing(sprintPath.get(i));
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("err");
                return 1;
            }
        }

        return 0;
    }
    //Executes matching of story to epic, issue to story, and writes to the original sprint file.
    public static void executeExcelParsing(String path) throws IOException {
        // Path names for Epic, Story, Issues, and the file to write to
        file = new FileInputStream(new File(path)); // This is the file being written to
        epicFile = new FileInputStream(new File(epicPath));
        storyFile = new FileInputStream(new File(storyPath));
        issueFile = new FileInputStream(new File(issuePath));
        //Opening and Initializing the excel workbooks and getting the desired sheet
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        XSSFWorkbook epicWorkbook = new XSSFWorkbook(epicFile);
        XSSFSheet epicSheet = epicWorkbook.getSheetAt(0);
        XSSFWorkbook storyWorkbook = new XSSFWorkbook(storyFile);
        XSSFSheet storySheet = storyWorkbook.getSheetAt(0);
        XSSFWorkbook issueWorkbook = new XSSFWorkbook(issueFile);
        XSSFSheet issueSheet = issueWorkbook.getSheetAt(0);

        //Retrieves every epic from the epic list and adds it to the epic array list
        //First Column(cell 0) will always be the CPMS number. 
        //Second Column(cell 1) is the ID which is unused
        //Third Column(cell 2) is the Epic Name
        for (int i = 1; i < epicSheet.getLastRowNum()+1; i++) {
            Row row = epicSheet.getRow(i);
            Cell cell = row.getCell(0);
            DataFormatter df = new DataFormatter();
            String CMPS = df.formatCellValue(cell);
            cell = row.getCell(2);
            String name = df.formatCellValue(cell);
            Epic epic = new Epic(CMPS, name);
            epicList.add(epic);
        }
        /*Retrieves every story from the story list, adds it to story list AND updates the epic it belongs to
         * First Column(cell 0) is the CMPS Number
         * Second Column(cell 1) is the ID which is unused
         * Third Column(cell 2) is the Story name, aka the summary field on the excel file
         * Fourth Column(cell 3) is the parent epic link. 
         */
        
        for (int i = 1; i < storySheet.getLastRowNum()+1; i++) {
            Row row = storySheet.getRow(i);
            Cell cell = row.getCell(0);
            DataFormatter df = new DataFormatter();
            String CMPS = df.formatCellValue(cell);
            cell = row.getCell(2);
            String name = df.formatCellValue(cell);
            cell = row.getCell(3);
            String parent = df.formatCellValue(cell);
            Story story = new Story(CMPS, name, parent);
            storyList.add(story);
            //Iterates throuhg the epic list and looks for the name of the parent. If found, adds the parent CMPS to the story
            for (int j = 0; j < epicList.size(); j++) {
                Epic epic = epicList.get(j);
                if (epic.getCMPS().equals(parent)) {
                    epic.addStory(story);
                }
            }
        }
        //Retrieve issue from issue sheet, add the CMPS number to the story list its attributed to.
        for (int i = 1; i < issueSheet.getLastRowNum()+1; i++) {
            Row row = issueSheet.getRow(i);
            Cell issueCell = row.getCell(0);
            Cell storyCell = row.getCell(1);
            String storyString = storyCell.getStringCellValue();
            String[] storyArray = storyCell.getStringCellValue().split("[, ]"); //Split at [, ] in case of more than one linked story
            if (storyCell.getStringCellValue().contains(",")) {
                for (int j = 0; j < storyArray.length; j++) {
                    if (j != 0) {
                        storyArray[j] = storyArray[j].substring(1);
                    }
                }
            }
            for (int j = 0; j < storyArray.length; j++) {
                storyString = storyArray[j];
                for (int k = 0; k < storyList.size(); k++) {
                    if (storyString.equals(storyList.get(k).getCMPS())) {
                        storyList.get(k).addIssue(issueCell.getStringCellValue());
                    }
                }
            }
        }

        //Write back to the sprint file
        for (int i = 1; i < sheet.getLastRowNum()+1; i++) {
            Row row = sheet.getRow(i);
            Cell cell = row.getCell(4);
            for (int j = 0; j < storyList.size(); j++) {
                if (storyList.get(j).getIssueList().contains(cell.getStringCellValue())) {

                    String parent = storyList.get(j).parentCMPS;
                    for (int k = 0; k < epicList.size(); k++) {
                        if (epicList.get(k).getCMPS().equals(parent)) {

                            cell = row.createCell(0);
                            cell.setCellValue(epicList.get(k).getCMPS());
                            cell = row.createCell(1);
                            cell.setCellValue(epicList.get(k).getName());
                            cell = row.createCell(2);
                            cell.setCellValue(storyList.get(j).getName());
                            cell = row.createCell(3);
                            cell.setCellValue(storyList.get(j).getCMPS());
                        }
                    }
                }
            }
        }
        file.close();
        FileOutputStream fileOut = new FileOutputStream(new File(path));
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
        epicWorkbook.close();
        storyWorkbook.close();
        issueWorkbook.close();
    }
}
