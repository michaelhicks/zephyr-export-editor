import java.util.ArrayList;


//Story Object: Holds the story number and name, the parent CMPS number (Epic) and
//an arraylist of linked issues.
public class Story {
    protected String name;
    protected String CMPS;
    protected String parentCMPS;
    public ArrayList<String> issueList = new ArrayList<String>();
    public Story(String CMPS, String name, String parentCMPS) {
        this.name = name;
        this.CMPS = CMPS;
        this.parentCMPS = parentCMPS;
    }

    public String getCMPS() {
        return CMPS;
    }

    public String getName() {
        return name;
    }

    public void addIssue(String issue) {
        if (!(issueList.contains(issue))) {
            issueList.add(issue);
        }
    }

    public ArrayList<String> getIssueList() {
        return issueList;
    }
}
