import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.*;

/*
 * FileChooserDemo.java uses these files:
 *   images/Open16.gif
 *   images/Save16.gif
 */
public class FileChooserDemo extends JPanel
        implements ActionListener {
    static JFrame frame;

    public static ArrayList<String> sprintPath = new ArrayList<String>();
    public static String epicPath = "";
    public static String storyPath = "";
    public static String issuePath = "";
    public static int counter = 0;
    public static int numFiles;

    static private final String newline = "\n";
    JButton sprintButton, epicButton, storyButton, issueButton, doneButton;
    JTextArea log;
    JFileChooser fc;

    public FileChooserDemo() {
        super(new BorderLayout());

        //Create the log first, because the action listeners
        //need to refer to it.
        log = new JTextArea(5,20);
        log.setMargin(new Insets(5,5,5,5));
        log.setEditable(false);
        JScrollPane logScrollPane = new JScrollPane(log);

        //Create a file chooser
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel Worksheets", "xlsx");
        fc = new JFileChooser();
        fc.setFileFilter(filter);
        JPanel buttonPanel = new JPanel(); //use FlowLayout

        for (int i = 0; i < numFiles; i++) {
            JButton b = new JButton("Sprint File " + (i+1));
            b.addActionListener(this);
            buttonPanel.add(b);
        }

        //Create the save button.  We use the image from the JLF
        //Graphics Repository (but we extracted it from the jar).
        epicButton = new JButton("Select the Epic File");
        epicButton.addActionListener(this);

        storyButton = new JButton("Select the Story File");
        storyButton.addActionListener(this);

        issueButton = new JButton("Select the Issue File");
        issueButton.addActionListener(this);
        //For layout purposes, put the buttons in a separate panel

        buttonPanel.add(epicButton);
        buttonPanel.add(storyButton);
        buttonPanel.add(issueButton);

        JPanel buttonPanel2 = new JPanel();
        doneButton = new JButton("Done");
        doneButton.addActionListener(this);
        buttonPanel2.add(doneButton);

        //Add the buttons and the log to this panel.
        add(buttonPanel, BorderLayout.PAGE_START);
        add(buttonPanel2, BorderLayout.PAGE_END);
        add(logScrollPane, BorderLayout.CENTER);
        log.append("If your file does not appear, make sure it is saved as a .xlsx file in Excel");
    }

    /*
     * actionPerformed:
     * When action performed, the source is retrieved and the correct action is taken. 
     */
    public void actionPerformed(ActionEvent e) {

        //Button clicked is the epic button.
        if (e.getSource() == epicButton) {
            int returnVal = fc.showOpenDialog(FileChooserDemo.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                //This is where a real application would open the file.
                log.append(file.getPath() + newline);
                epicPath = file.getPath();
                epicButton.setEnabled(false);
                counter++;
            } else {
                log.append("Open command cancelled by user." + newline);
            }
            log.setCaretPosition(log.getDocument().getLength());
        } else if (e.getSource() == storyButton) {
            int returnVal = fc.showOpenDialog(FileChooserDemo.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                //This is where a real application would open the file.
                log.append(file.getPath() + newline);
                storyPath = file.getPath();
                storyButton.setEnabled(false);
                counter++;
            } else {
                log.append("Open command cancelled by user." + newline);
            }
            log.setCaretPosition(log.getDocument().getLength());
        } else if (e.getSource() == issueButton) {
            int returnVal = fc.showOpenDialog(FileChooserDemo.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                //This is where a real application would open the file.
                log.append(file.getPath() + newline);
                issuePath = file.getPath();
                issueButton.setEnabled(false);
                counter++;
            } else {
                log.append("Open command cancelled by user." + newline);
            }
            log.setCaretPosition(log.getDocument().getLength());
        } else if (e.getSource() == doneButton) {
            frame.dispose();
            int retValue = FileRead.file_init(sprintPath, epicPath, storyPath, issuePath);
            if (retValue == 0) {
                JOptionPane.showMessageDialog(null, "Process Done");
            } else {
                JOptionPane.showMessageDialog(null, "An error occurred and the process failed.");
            }
        } else {
            Object b = e.getSource();
            JButton jb = (JButton) b;
            int returnVal = fc.showOpenDialog(FileChooserDemo.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                log.append(file.getPath() + newline);
                sprintPath.add(file.getPath());
                jb.setEnabled(false);
            }
        }
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        frame = new JFrame("FileChooserDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add content to the window.
        frame.add(new FileChooserDemo());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        if (JOptionPane.showConfirmDialog(null, "Are you sure you have run the Excel Macro to reformatSheet\n and that the excel file is saved with the .xlsx extension?", "WARNING",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            String filesInput;
            filesInput = JOptionPane.showInputDialog("How many files do you want to export?");
            numFiles = Integer.parseInt(filesInput);
            // yes option
        } else {
            // no option
            return;
        }
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                createAndShowGUI();
            }
        });
    }
}